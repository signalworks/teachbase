import expect from 'expect'
import React from 'react'
import TestUtils from 'react-addons-test-utils'
import { App } from '../../src/containers/App'



function setup() {
  let props = {
    dispatch: expect.createSpy(),
    quizInfo: {},
    questionNumber: 1,
    question: {},
    answers: [],
    quizTimer: 0,
    answerTimer: 0,
    results: [],
    windowVisible: false
  };

  let renderer = TestUtils.createRenderer();
  renderer.render( <App {...props} /> );
  let output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  }
}

describe('containers', () => {
  describe('App', () => {
    it('should render correctly', () => {
      const output = setup().output;

      expect(output.type).toBe('div');
      expect(output.props.className).toBe('app');

      expect(output.props.children[0].type.name).toBe('Header');
      expect(output.props.children[1].props.className).toBe('progress');
      expect(output.props.children[2].type.name).toBe('Question');
      expect(output.props.children[3].type.name).toBe('Timer');
      expect(output.props.children[4].type.name).toBe('AnswerList');
    });
  })
});