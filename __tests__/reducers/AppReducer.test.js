import expect from 'expect'

import reducer from '../../src/reducers/AppReducer'
import * as types from '../../src/constants/AppConstants'


describe('reducer', () => {
  it('should return the initial state', () => {
    expect( reducer(undefined, {}) ).toEqual({
        quizInfo: {},
        questionNumber: 1,
        question: {},
        answers: [],
        quizTimer: 0,
        answerTimer: 0,
        results: [],
        isFinish: false,
        windowVisible: false
    })
  });

  it('should handle RECV_QUIZ_INFO', () => {
    const quizInfo = {
      name: 'ТЕСТ',
      count: 4
    };
    expect(
      reducer({}, {
        type: types.RECV_QUIZ_INFO,
        payload: quizInfo
      })
    ).toEqual({
      quizInfo: quizInfo,
      answers: [[], [], [], [], []],
      results: [false, false, false, false]
    });
  });

  it('should handle RECV_QUESTION', () => {
    const question = {
      type: 'text',
      imageUrl: 'https://avatars3.githubusercontent.com/u/2551688?v=3&s=52',
      text: 'text',
      variants: []
    };
    expect(
      reducer({}, {
        type: types.RECV_QUESTION,
        payload: question
      })
    ).toEqual({
      question: question,
      answerTimer: 0
    });
  });

  it('should handle CHANGE_QUESTION_NUMBER 2', () => {
    const number = 2;
    expect(
      reducer({}, {
        type: types.CHANGE_QUESTION_NUMBER,
        payload: number
      })
    ).toEqual({ questionNumber: number });
  });

  it('should handle CHANGE_ANSWER [\'2\']', () => {
    const answer = {
      answers: ['2'],
      number: 1
    };
    expect(
      reducer({ answers: [] }, {
        type: types.CHANGE_ANSWER,
        payload: answer
      })
    ).toEqual({ answers: [, answer.answers] });
  });

  it('should handle RECV_ANSWER 2', () => {
    expect(
      reducer({ results: [false] }, {
        type: types.RECV_ANSWER,
        payload: {
          number: 2,
          response: true
        }
      })
    ).toEqual({ results: [false, true] });
  });

  it('should handle UPDATE_QUIZ_TIMER', () => {
    expect(
      reducer({ quizTimer: 2 }, {
        type: types.UPDATE_QUIZ_TIMER
      })
    ).toEqual({ quizTimer: 3 });
  });

  it('should handle UPDATE_ANSWER_TIMER', () => {
    expect(
      reducer({ answerTimer: 2 }, {
        type: types.UPDATE_ANSWER_TIMER
      })
    ).toEqual({ answerTimer: 3 });
  });

  it('should handle CLEAR_ANSWER_TIMER', () => {
    expect(
      reducer({ answerTimer: 2 }, {
        type: types.CLEAR_ANSWER_TIMER
      })
    ).toEqual({ answerTimer: 0 });
  });

  it('should handle TOGGLE_WINDOW', () => {
    expect(
      reducer({ windowVisible: true }, {
        type: types.TOGGLE_WINDOW
      })
    ).toEqual({ windowVisible: false });
  });
});