import expect from 'expect'
import React from 'react'
import TestUtils from 'react-addons-test-utils'
import Timer from '../../src/components/Timer'



function setup(isLast=false) {
  let props = {
    dispatch: expect.createSpy(),
    timer: 156,
    send: expect.createSpy(),
    finish: expect.createSpy(),
    isLast: isLast
  };

  let renderer = TestUtils.createRenderer();
  renderer.render( <Timer {...props} /> );
  let output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  }
}

describe('components', () => {
  describe('Timer', () => {
    it('should render correctly', () => {
      const { output } = setup();

      expect(output.type).toBe('div');
      expect(output.props.className).toBe('timer');

      expect(output.props.children[0].props.children[1]).toBe('00:02:36');
      expect(output.props.children[1].props.children.trim()).toBe('ответить');
    });

    it('should last render correctly', () => {
      const { output } = setup(true);

      expect(output.type).toBe('div');
      expect(output.props.className).toBe('timer');

      expect(output.props.children[0].props.children[1]).toBe('00:02:36');
      expect(output.props.children[1].props.children.trim()).toBe('завершить');
    });

    it('button callback', () => {
      const { output, props } = setup();
      const btn = output.props.children[1];
      
      btn.props.onClick();
      expect(props.send.calls.length).toBe(1);
    })
  })
});