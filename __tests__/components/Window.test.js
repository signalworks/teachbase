import expect from 'expect'
import React from 'react'
import TestUtils from 'react-addons-test-utils'
import Window from '../../src/components/Window'



function setup() {
  let props = {
    hide: expect.createSpy(),
    result: '20%'
  };

  let renderer = TestUtils.createRenderer();
  renderer.render( <Window {...props} /> );
  let output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  }
}

describe('components', () => {
  describe('Window', () => {
    it('should render correctly', () => {
      const { output, props } = setup(true);

      expect(output.props.children[0].props.className).toBe('shadow');
      expect(output.props.children[1].props.className).toBe('window');
      expect(output.props.children[1].props.children[2].props.children.join('').trim()).toBe('Ваш результат - 20%');
    });

    it('button callback', () => {
      const { output, props } = setup(true);
      
      output.props.children[0].props.onClick();
      expect(props.hide.calls.length).toBe(1);
      
      let btn = output.props.children[1].props.children[3];
      
      btn.props.onClick();
      expect(props.hide.calls.length).toBe(2);
    })
  })
});