import expect from 'expect'
import React from 'react'
import TestUtils from 'react-addons-test-utils'

import AnswerList from '../../src/components/AnswerList'


function setup(type='text', variants=[]) {
  let props = {
    number: 1,
    dispatch: expect.createSpy(),
    type: type,
    variants: variants,
    answers: []
  };

  let renderer = TestUtils.createRenderer();
  renderer.render(<AnswerList {...props} />);
  let output = renderer.getRenderOutput();
  let mounted = renderer.getMountedInstance();

  return {
    props,
    output,
    mounted,
    renderer
  }
}

describe('components', () => {
  describe('AnswerList', () => {
    it('should render correctly', () => {
      const { output } = setup();

      expect(output.type).toBe('div');
      expect(output.props.className).toBe('variants');

      expect(output.props.children.type).toBe('input');
      expect(output.props.children.props.type).toBe('text');
    });

    it('should last render correctly', () => {
      const { output } = setup('single', ['1', '2', '3']);
      let radio = output.props.children[1].props.children[0];

      expect(output.props.children.length).toBe(3);
      expect(radio.props.className).toBe('radio');
    });

    it('change radio callback', () => {
      const { output, props } = setup('multiple', ['1', '2', '3']);
      let radio = output.props.children[1].props.children[0];

      radio.props.onClick({
        target: {
          value: '2',
          checked: true
        }
      });
      expect(props.dispatch.calls.length).toBe(1);
    });

    it('change text callback', () => {
      const { output, props } = setup();
      let input = output.props.children;

      input.props.onChange({
        target: {
          value: 'a'
       }
      });
      expect(props.dispatch.calls.length).toBe(1);
    });
  })
});