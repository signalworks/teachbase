import expect from 'expect'
import React from 'react'
import TestUtils from 'react-addons-test-utils'
import Header from '../../src/components/Header'



function setup() {
  let props = {
    dispatch: expect.createSpy(),
    number: 1,
    info: {
      title: 'ТЕСТ',
      count: 4
    }
  };

  let renderer = TestUtils.createRenderer();
  renderer.render( <Header {...props} /> );
  let output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  }
}

describe('components', () => {
  describe('Header', () => {
    it('should render correctly', () => {
      const { output, props } = setup();
      const result = `${ props.info.title } (${ props.number } / ${ props.info.count })`;

      expect(output.type).toBe('div');
      expect(output.props.className).toBe('header');

      let bold = output.props.children[1];

      expect(bold.type).toBe('bold');
      expect(bold.props.children.join('').trim()).toBe(result);
    });

    it('navigation callbacks', () => {
      const { output, props } = setup();
      let [ back, next ] = output.props.children[2].props.children;
      
      back.props.onClick();
      expect(props.dispatch.calls.length).toBe(0);
      
      next.props.onClick();
      expect(props.dispatch.calls.length).toBe(2);
      expect(props.dispatch.calls[0].arguments[0].payload).toBe(2);
    })
  })
});