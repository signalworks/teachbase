import expect from 'expect'
import React from 'react'
import TestUtils from 'react-addons-test-utils'

import Question from '../../src/components/Question'


function setup(imageUrl='') {
  let props = {
    dispatch: expect.createSpy(),
    question: {
      type: 'text',
      imageUrl: imageUrl,
      text: 'text',
      variants: []
    }
  };

  let renderer = TestUtils.createRenderer();
  renderer.render( <Question {...props} /> );
  let output = renderer.getRenderOutput();

  return {
    props,
    output,
    renderer
  }
}

describe('components', () => {
  describe('Question', () => {
    it('should render correctly', () => {
      const { output, props } = setup();

      expect(output.type).toBe('div');
      expect(output.props.className).toBe('question');

      expect(output.props.children[1].props.children.join('').trim()).toBe(props.question.text);
    });

    it('should last render correctly', () => {
      const { output, props } = setup('');

      expect(output.props.children[0].type).toBe('div');
    });
  })
});