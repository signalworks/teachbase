import expect from 'expect'
import * as actions from '../../src/actions'
import * as types from '../../src/constants/AppConstants'


describe('actions' , () => {
  describe('getQuestion', () => {
    it('get first question', () => {
      const expectedAction = {
        type: types.RECV_QUESTION,
        payload: {
          type: 'text',
          imageUrl: {},
          text: 'text',
          variants: []
        }
      };
      expect( actions.getQuestion(1) ).toEqual(expectedAction);
    });

    it('get nothing question', () => {
      const expectedAction = {
        type: types.RECV_QUESTION,
        payload: null
      };
      expect( actions.getQuestion(25) ).toEqual(expectedAction);
    })
  });

  describe('sendAnswer', () => {
    it('send true first', () => {
      const expectedAction = {
        type: types.RECV_ANSWER,
        payload: {
          response: true,
          number: 1
        }
      };
      expect( actions.sendAnswer(['test'], 1) ).toEqual(expectedAction);
    });

    it('send other true first', () => {
      const expectedAction = {
        type: types.RECV_ANSWER,
        payload: {
          response: true,
          number: 1
        }
      };
      expect( actions.sendAnswer(['   tEsT  '], 1) ).toEqual(expectedAction);
    });

    it('send false first', () => {
      const expectedAction = {
        type: types.RECV_ANSWER,
        payload: {
          response: false,
          number: 1
        }
      };
      expect( actions.sendAnswer(['false'], 1) ).toEqual(expectedAction);
    });
  });

  describe('getQuizInfo', () => {
    it('get info', () => {
      const expectedAction = {
        type: types.RECV_QUIZ_INFO,
        payload: {
          title: 'ТЕСТ',
          count: 4,
          questions: [
            'text',
            'single',
            'multiple',
            'drag'
          ]
        }
      };
      expect( actions.getQuizInfo() ).toEqual(expectedAction);
    });
  });

  describe('changeQuestionNumber', () => {
    it('change 1', () => {
      const number = 1;
      const expectedAction = {
        type: types.CHANGE_QUESTION_NUMBER,
        payload: number
      };
      expect( actions.changeQuestionNumber(number) ).toEqual(expectedAction);
    });
  });

  describe('updateQuizTimer', () => {
    it('update', () => {
      const expectedAction = {
        type: types.UPDATE_QUIZ_TIMER
      };
      expect( actions.updateQuizTimer() ).toEqual(expectedAction);
    });
  });

  describe('updateAnswerTimer', () => {
    it('update', () => {
      const expectedAction = {
        type: types.UPDATE_ANSWER_TIMER
      };
      expect( actions.updateAnswerTimer() ).toEqual(expectedAction);
    });

    describe('changeAnswer', () => {
      it('change [\'123\']', () => {
        const answer = {
          answers: [ '123' ],
          number: 2
        };
        const expectedAction = {
          type: types.CHANGE_ANSWER,
          payload: {
            answers: [ '123' ],
            number: 2
          }
        };
        expect( actions.changeAnswer(answer.answers, answer.number) ).toEqual(expectedAction);
      });
    });

    describe('toggleWindow', () => {
      it('toggle', () => {
        const expectedAction = {
          type: types.TOGGLE_WINDOW
        };
        expect( actions.toggleWindow() ).toEqual(expectedAction);
      });
    });
  });
});