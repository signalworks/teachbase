import React, { PropTypes, Component } from 'react'

import { updateQuizTimer, updateAnswerTimer } from '../actions'

import '../styles/Timer.css'
import imgTimer from '../images/timer.png'


export default class Timer extends Component {
  shouldComponentUpdate(props, state) {
    if (this.props.timer != props.timer) {
      return true;
    }

    if (this.props.isLast != props.isLast) {
      return true;
    }
    
    return false;
  }
  
  componentDidMount() {
    const { dispatch } = this.props;
    
    let timer = setInterval(() => {
      dispatch( updateQuizTimer() );
      dispatch( updateAnswerTimer() );
    }, 1000);
    
    this.setState({ timer: timer });
  }
  
  componentWillUnmount() {
    clearInterval(this.state.timer);
  }
  
  render() {
    const { timer, send, finish, isLast } = this.props;
    const date = new Date;
    date.setHours(timer / 3600, timer / 60, timer % 60, 0);

    const nextBtn = <button className='submit' onClick={ send } > ответить </button>;
    const finishBtn = <button className='submit' onClick={ finish } > завершить </button>;
    
    return <div className='timer'>
      <span>
        <img src={ imgTimer } />
        {
          date.toLocaleString("ru", {
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric' })
        }
      </span>
      { isLast ? finishBtn : nextBtn }
    </div>
  }
}