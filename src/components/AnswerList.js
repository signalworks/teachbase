import React, { PropTypes, Component } from 'react'

import { changeAnswer } from '../actions'
import AnswerListDrag from './AnswerListDrag'

import '../styles/AnswerList.css'


export default class AnswerList extends Component {
  shouldComponentUpdate(props, state) {
    if (this.state != state) {
      return true;
    }
    
    if (this.props.variants != props.variants) {
      return true;
    }
    
    if (this.props.type != props.type) {
      return true;
    }

    if (this.props.answers != props.answers) {
      return true;
    }

    return false;
  }
  
  change(e) {
    const { dispatch, number, type } = this.props;
    const { checked, value } = e.target;
    const index = this.props.answers.indexOf(value);
    const answers = this.props.answers.slice();

    if (checked && index == -1) {
      if (type == 'multiple') {
        answers.push(value);
        dispatch( changeAnswer(answers, number) );
      } else {
        dispatch( changeAnswer([ value ], number) );
      }
    } else if (!checked && index != -1 && type == 'multiple') {
      answers.splice(index, 1);
      dispatch( changeAnswer(answers, number) );
    }
  }

  changeText(e) {
    const { dispatch, number } = this.props;

    dispatch( changeAnswer([ e.target.value ], number) );
  }

  render() {
    const { type, variants, answers } = this.props;
    const typeAndClass = (type == 'single') ? 'radio' : 'checkbox';
    const empty = <div></div>;

    if (!type) {
      return empty;
    }

    if (type == 'drag') {
      return <AnswerListDrag { ...this.props } />
    }

    if (type == 'text') {
      return <div className='variants'>
        <input
          type='text'
          placeholder='ответ'
          value={ answers[0] }
          onChange={ this.changeText.bind(this) }
        />
      </div>
    }

    return <div className='variants'>
      {variants.map( (v, i) => (
        <label key={ i } >
          <input
            className={ typeAndClass }
            type={ typeAndClass }
            value={ v }
            checked={ answers.indexOf(v) != -1 }
            onClick={ this.change.bind(this) }
          />
          { v }
          <br/>
        </label>
      ))}
    </div>
  }
}