import React, { PropTypes, Component } from 'react'

import '../styles/Question.css'


export default class Question extends Component {
  shouldComponentUpdate(props, state) {
    if (this.props.question != props.question) {
      return true;
    }
    
    return false;
  }

  render() {
    const { imageUrl, text } = this.props.question;
    
    return <div className='question'>
      {imageUrl?(
        <center>
          <img align='center' className='questionImage' src={ imageUrl } />
        </center>
      ):(
        <div></div>
      )}
      <p> { text } </p>
    </div>
  }
}