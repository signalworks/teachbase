import React, { PropTypes, Component } from 'react'
import _ from 'lodash/array'

import { toggleWindow, resetApp, getQuizInfo, getQuestion} from '../actions'
import Window from '../components/Window'

import '../styles/FinishScreen.css'

import imgTimer from '../images/timer.png'
import imgCalendar from '../images/calendar.png'
import imgRight from '../images/right.png'
import imgWrong from '../images/wrong.png'


export default class FinishScreen extends Component {
  componentDidMount() {
    this.props.dispatch( toggleWindow() );
  }

  shouldComponentUpdate(props, state) {
    if (this.props.windowVisible != props.windowVisible) {
      return true;
    }

    return false;
  }

  hide () {
    this.props.dispatch( toggleWindow() );
  }
  
  close() {
    const { dispatch } = this.props;

    dispatch( resetApp() );
    dispatch( getQuizInfo() );
    dispatch( getQuestion(1) );
  }
  
  render() {
    const { timer, windowVisible, results, count, questions } = this.props;
    const result =  (results ? results.reduce( (a, b) => (a + b) ) : 0) / count * 100 + '%';
    const dateTimer = new Date;
    dateTimer.setHours(timer / 3600, timer / 60, timer % 60, 0);
    const dateNow = new Date();

    let shadow = windowVisible ? (
      <Window
        hide={ this.hide.bind(this) }
        result={ result }
      />
    ) : <div></div>;

    
    return <div className='finish'>
      { shadow }
      <div className='head'>
        <button
          className='back'
          onClick={ this.close.bind(this) }
        >
          &lt;
        </button>
        <span>
          КАРТОЧКА ТЕСТА
        </span>
      </div>

      <div className='content'>
        Ответы:
      </div>

      {results.map( (v, i) => (
        <div
          key={ i }
          className={v ? 'answer-right' : 'answer-wrong'}
        >
          <img src={ v ? imgRight : imgWrong }/>
          Вопрос { i + 1 }: { questions[i] }
        </div>
      ))}

      <div className='block'>
        <img src={ imgCalendar } />
        <span>
          <div className='title'>
            Время прохождения
          </div>
          <div className='content'>{
            dateTimer.toLocaleString("ru", {
              hour: 'numeric',
              minute: 'numeric',
              second: 'numeric'
            })
          }</div>
        </span>
      </div>

      <div className='block'>
        <img src={ imgTimer } />
        <span>
          <div className='title'>
            Дата прохождения
          </div>
          <div className='content'>{
            dateNow.toLocaleString("ru", {
              month: 'long',
              day: 'numeric',
              weekday: 'long'
            })
          }</div>
        </span>
      </div>
    </div>
  }
}