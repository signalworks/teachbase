import React, { PropTypes, Component } from 'react'

import { changeAnswer } from '../actions'

import '../styles/AnswerListDrag.css'


export default class AnswerList extends Component {
  constructor() {
    super();
    this.state = {
      pressed: null,
      place: null,
      mouse: [],
      delta: []
    };
  }

  componentDidMount() {
    window.addEventListener('touchmove', this.handleTouchMove.bind(this));
    window.addEventListener('touchend', this.handleMouseUp.bind(this));
    window.addEventListener('mousemove', this.handleMouseMove.bind(this));
    window.addEventListener('mouseup', this.handleMouseUp.bind(this));
  }

  componentWillReceiveProps(props, state) {
    if (props.variants != this.props.variants) {
      this.setState({
        pressed: null,
        place: null
      });
    }
  }

  shouldComponentUpdate(props, state) {
    if (this.state != state) {
      return true;
    }

    if (this.props.variants != props.variants) {
      return true;
    }

    if (this.props.answers != props.answers) {
      return true;
    }

    return false;
  }

  handleMouseDown(i, e, touch=false) {
    const { clientX: x, clientY: y } = e;
    const { left, top } = e.target.getBoundingClientRect();
    const { answers } = this.props;
    const { dispatch, variants, number } = this.props;
    const index = answers.indexOf(variants[0][i]);

    if (!touch) {
      e.preventDefault();
    }

    if (index !== -1) {
      delete answers[index];
      dispatch( changeAnswer(answers, number) );
    }

    this.setState({
      pressed: i,
      mouse: [x, y],
      delta: [x - left, y - top]
    });
  }

  handleMouseMove(e) {
    const { clientX: x, clientY: y } = e;
    const { pressed } = this.state;
    const elem = document.elementFromPoint(x, y);
    let place = null;

    if (pressed !== null) {
      if (elem.getAttribute('class') == 'drop') {
        place = elem.getAttribute('name');
      }

      this.setState({
        mouse: [x, y],
        place: place
      });
    }
  }

  handleMouseUp() {
    const { dispatch, variants, number, answers } = this.props;
    const { pressed, place } = this.state;

    if (pressed !== null) {
      if (place !== null) {
        answers[place] = variants[0][pressed];
        dispatch( changeAnswer(answers, number) );
      }

      this.setState({
        pressed: null,
        place: null,
        mouse: []
      });
    }
  }

  handleTouchStart(key, e) {
    this.handleMouseDown(key, e.touches[0], true);
  }

  handleTouchMove(e) {
    const { pressed } = this.state;

    if (pressed !== null) {
      e.preventDefault();
      this.handleMouseMove(e.touches[0]);
    }
  }

  render() {
    const { variants, answers } = this.props;
    const { pressed, place, mouse, delta } = this.state;
    const empty = <div></div>;

    if (!variants) {
      return empty;
    }

    return <div className='variants'>
      {variants[0].map( (v, i) => (
        (answers.indexOf(v) == -1) ? (
          <button
            key={ i }
            className='drag'
            onMouseDown={ this.handleMouseDown.bind(this, i) }
            onTouchStart={ this.handleTouchStart.bind(this, i) }
            style={ (pressed == i) ? {
                position: 'absolute',
                top: mouse[1] - delta[1],
                left: mouse[0] - delta[0]
              } : {} }
          >
            { v }
          </button>
        ) : (<div key={ i } ></div>)
      ))}

      {variants[1].map( (v, i) => (
        <div key={ i } className='place'>
          <span className='caption'> { v } </span>
          {(answers[i] != undefined) ? (
            <button
              className='drag'
              onMouseDown={ this.handleMouseDown.bind(this, variants[0].indexOf(answers[i])) }
              onTouchStart={ this.handleTouchStart.bind(this, variants[0].indexOf(answers[i])) }
            >
              { answers[i] }
            </button>
          ) : (
            <span
              className='drop'
              name={ i }
              style={ (place == i) ? { color: '#FF0000' } : {} }
            >
              place her
            </span>
          )}
        </div>
      ))}
    </div>
  }
}
