import React, { PropTypes, Component } from 'react'

import { changeQuestionNumber, getQuizInfo,
  getQuestion, resetApp } from '../actions'

import '../styles/Header.css'


export default class Header extends Component {
  shouldComponentUpdate(props, state) {
    if (this.props.number != props.number) {
      return true;
    }

    if (this.props.info != props.info) {
      return true;
    }

    return false;
  }

  reset() {
    const { dispatch } = this.props;

    dispatch( resetApp() );
    dispatch( getQuizInfo() );
    dispatch( getQuestion(1) );
  }

  back() {
    const { dispatch, number } = this.props;
    
    if (number > 1) {
      dispatch( changeQuestionNumber(number - 1) );
      dispatch( getQuestion(number - 1) );
    }
  }
  
  next() {
    const { dispatch, number } = this.props;
    const { count } = this.props.info;
    
    if (number < count) {
      dispatch( changeQuestionNumber(number + 1) );
      dispatch( getQuestion(number + 1) );
    }
  }
  
  render() {
    const { title, count } = this.props.info;
    const number = this.props.number;
    
    return <div className='header'>
      <button className='btnClose' onClick={ this.reset.bind(this) }> X </button>
      <bold> { title } ({ number } / { count }) </bold>
      <span className='Nav'>
        <button
          onClick={ this.back.bind(this) }
          style={ (number == 1) ? {
            opacity: '0.2'
          } : {}}
        >
          &lt;
        </button>
        <button
          onClick={ this.next.bind(this) }
          style={ (number == count) ? {
            opacity: '0.2'
          } : {}}
        >
          &gt;
        </button>
      </span>
    </div>
  }
}