import React, { PropTypes, Component } from 'react'

import { toggleWindow } from '../actions'

import '../styles/Window.css'


export default class Window extends Component {
  render() {
    const { hide, result } = this.props;

    return <div>
      <div
        className='shadow'
        onClick={ hide }
      >
      </div>
      <div
        className='window'
      >
        <h1> Поздравляем! </h1>
        <p> Тест пройден! </p>
        <p> Ваш результат - { result } </p>
        <button
          onClick={ hide }
          className='hide'
        >
          закрыть
        </button>
      </div>
    </div>
  }
}