import React, { Component } from 'react'
import { connect } from 'react-redux'

import { getQuizInfo, getQuestion,
  changeQuestionNumber, sendAnswer, toggleFinish } from '../actions'

import Header from '../components/Header'
import Question from '../components/Question'
import Timer from '../components/Timer'
import AnswerList from '../components/AnswerList'
import FinishScreen from '../components/FinishScreen'

import '../styles/App.css'


export class App extends Component {
  componentWillMount() {
    this.props.dispatch( getQuizInfo() );
    this.props.dispatch( getQuestion(1) );
  }

  send() {
    const { dispatch, questionNumber, answers, answerTimer }= this.props;
    const { count } = this.props.quizInfo;

    if (!answers[questionNumber]) {
      return;
    }
    
    dispatch( sendAnswer(answers[questionNumber], questionNumber, answerTimer) );

    if (questionNumber < count) {
      dispatch( changeQuestionNumber(questionNumber + 1) );
      dispatch( getQuestion(questionNumber + 1) );
    }
  }
  
  sendFinish() {
    this.send();
    this.props.dispatch( toggleFinish() );
  }
  
  render() {
    const { quizInfo, questionNumber, question, quizTimer,
      dispatch, results, windowVisible, answers, isFinish } = this.props;
    
    if (!quizInfo || !question ) {
      return <div></div>
    }

    if (isFinish) {
      return <div className='app'>
        <FinishScreen
          dispatch={ dispatch }
          timer={ quizTimer }
          windowVisible={ windowVisible }
          results={ results }
          count={ quizInfo.count }
          questions={ quizInfo.questions }
        />
      </div>
    }
    
    return <div className='app'>
      <Header
        dispatch={ dispatch }
        info={ quizInfo }
        number={ questionNumber }
      />
      
      <div
        className='progress'
        style={{
          width: questionNumber  / quizInfo.count * 100 + '%'
        }}
      >
      </div>
      
      <Question
        dispatch={ dispatch }
        question={ question }
      />
      
      <Timer
        dispatch={ dispatch }
        timer={ quizTimer }
        send={ this.send.bind(this) }
        finish={ this.sendFinish.bind(this) }
        isLast={ questionNumber == quizInfo.count }
      />
      
      <AnswerList
        dispatch={ dispatch }
        number={ questionNumber }
        answers={ answers[questionNumber] }
        type={ question.type }
        variants={ question.variants }
      />
    </div>
  }
}

function mapStateToProps(state) {
  return {
    quizInfo: state.quizInfo,
    questionNumber: state.questionNumber,
    question: state.question,
    quizTimer: state.quizTimer,
    answers: state.answers,
    answerTimer: state.answerTimer,
    results: state.results,
    isFinish: state.isFinish,
    windowVisible: state.windowVisible
  }
}

export default connect(mapStateToProps)(App);