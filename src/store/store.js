import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'

import rootReducer from '../reducers/AppReducer'

const logger = createLogger({ collapsed: true });

export default function configureStore(initialState) {
  const store = createStore(rootReducer, initialState, applyMiddleware(thunk, logger));

  if (module.hot) {
    module.hot.accept('../reducers/AppReducer', () => {
      const nextRootReducer = require('../reducers/AppReducer');
      store.replaceReducer(nextRootReducer);
    })
  }

  return store;
}