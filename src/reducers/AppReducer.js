import {
  RECV_QUIZ_INFO,
  RECV_QUESTION,
  CHANGE_QUESTION_NUMBER,
  CHANGE_ANSWER,
  RECV_ANSWER,
  UPDATE_QUIZ_TIMER,
  UPDATE_ANSWER_TIMER,
  CLEAR_ANSWER_TIMER,
  TOGGLE_FINISH,
  TOGGLE_WINDOW,
  RESET_APP
} from '../constants/AppConstants'

import _ from 'lodash/array'

const initialState = {
  quizInfo: {},
  questionNumber: 1,
  question: {},
  answers: [],
  quizTimer: 0,
  answerTimer: 0,
  results: [],
  isFinish: false,
  windowVisible: false
};

export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case RECV_QUIZ_INFO:
      return {
        ...state,
        quizInfo: action.payload,
        answers: _.fill(Array(action.payload.count + 1), []),
        results: _.fill(Array(action.payload.count), false)
      };

    case RECV_QUESTION:
      return { ...state, question: action.payload, answerTimer: 0 };
    
    case CHANGE_QUESTION_NUMBER:
      return { ...state, questionNumber: action.payload };

    case CHANGE_ANSWER: {
      let newAnswers = state.answers.slice();
      newAnswers[action.payload.number] = action.payload.answers;
      return { ...state, answers: newAnswers };
    }

    case RECV_ANSWER:
      state.results[action.payload.number - 1] = action.payload.response;
      return { ...state, results: state.results };
    
    case UPDATE_QUIZ_TIMER:
      return { ...state, quizTimer: ++state.quizTimer };
          
    case UPDATE_ANSWER_TIMER:
      return { ...state, answerTimer: ++state.answerTimer };
    
    case CLEAR_ANSWER_TIMER:
      return { ...state, answerTimer: 0 };
    
    case TOGGLE_FINISH:
      return{ ...state, isFinish: !state.isFinish };
    
    case TOGGLE_WINDOW:
      return { ...state, windowVisible: !state.windowVisible };
    
    case RESET_APP:
      return {
        quizInfo: {},
        questionNumber: 1,
        question: {},
        answers: [],
        quizTimer: 0,
        answerTimer: 0,
        results: [],
        isFinish: false,
        windowVisible: false
      };
    
    default:
      return state;
  }
}
