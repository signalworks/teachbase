import {
  RECV_QUIZ_INFO,
  RECV_QUESTION,
  CHANGE_QUESTION_NUMBER,
  CHANGE_ANSWER,
  RECV_ANSWER,
  UPDATE_QUIZ_TIMER,
  UPDATE_ANSWER_TIMER,
  TOGGLE_FINISH,
  TOGGLE_WINDOW,
  RESET_APP
} from '../constants/AppConstants'

import beachImg from '../images/beach.jpg'


export function getQuizInfo() {
  return {
    type: RECV_QUIZ_INFO,
    payload: {
      title: 'ТЕСТ',
      count: 4,
      questions: [
        'text',
        'single',
        'multiple',
        'drag'
      ]
    }
  };
}

export function getQuestion(number) {
  let question;
  
  switch (number) {
    case 1:
      question = {
        type: 'text',
        imageUrl: beachImg,
        text: 'text',
        variants: []
      };
      break;
    case 2:
      question = {
        type: 'single',
        imageUrl: '',
        text: 'singe',
        variants: [
          '1',
          '2',
          '3'
        ]
      };
      break;
    case 3:
      question = {
        type: 'multiple',
        imageUrl: '',
        text: 'multiple',
        variants: [
          '4',
          '5',
          '6'
        ]
      };
      break;
    case 4:
      question = {
        type: 'drag',
        imageUrl: '',
        text: 'drag',
        variants: [[
            '2',
            '1',
            '3'
          ],
          [
            'первый',
            'второй',
            'третий'
          ]]
      };
      break;
    default:
      question = null;
  }

  return {
    type: RECV_QUESTION,
    payload: question
  };
}

export function changeQuestionNumber(number) {
  return { 
    type: CHANGE_QUESTION_NUMBER,
    payload: number
  };
}

export function updateQuizTimer() {
  return {
    type: UPDATE_QUIZ_TIMER
  };
}

export function updateAnswerTimer() {
  return {
    type: UPDATE_ANSWER_TIMER
  };
}

export function changeAnswer(answer, number) {
  return {
    type: CHANGE_ANSWER,
    payload: {
      answers: answer,
      number: number
    }
  };
}

export function sendAnswer(answer, questionNumber, timer) {
  let response = false;
  
  switch (questionNumber) {
    case 1:
      if (answer[0].trim().toLowerCase() == 'test') {
        response = true;
      }
      break;
    case 2:
      if (answer[0] == '2') {
        response = true;
      }
      break;
    case 3:
      answer.sort();
      if (answer.length == 2 && answer[0] == '4' && answer[1] == '6') {
        response = true;
      }
      break;
    case 4:
      if (answer.length == 3 && answer[0] == '1' && answer[1] == '2' && answer[2] == '3') {
        response = true;
      }
      break;
  }
  
  return {
    type: RECV_ANSWER,
    payload: {
      response: response,
      number: questionNumber
    }
  };
}

export function toggleFinish() {
  return {
    type: TOGGLE_FINISH
  };
}

export function toggleWindow() {
  return {
    type: TOGGLE_WINDOW
  };
}

export function resetApp() {
  return {
    type: RESET_APP
  };
}